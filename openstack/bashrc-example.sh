#!/bin/bash

export OS_AUTH_TYPE="password"
# user+tenant information taken from myworkplace (see My credentials)
export OS_USERNAME="14xxxxx OTC00000000001000000xxx"
# tenant information from myworkplace
export OS_USER_DOMAIN_NAME="OTC00000000001000000xxx"
export OS_PASSWORD="<API credentials from My credentials>Generate>"
# Only change these for a different region
export OS_TENANT_NAME=eu-de 
export OS_PROJECT_NAME=eu-de
export OS_AUTH_URL=https://iam.eu-de.otc.t-systems.com/v3 
export OS_CACERT=$HOME/otc_certs.pem      #adapt path or copy pem file
# No changes needed beyond this point
export OS_ENDPOINT_TYPE=publicURL 
export OS_IDENTITY_API_VERSION=3 
export OS_IMAGE_API_VERSION=2
export OS_VOLUME_API_VERSION=2 

